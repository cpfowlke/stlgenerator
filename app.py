#!flask/bin/python
from flask import Flask
from flask import request
from classes.STLObject import Generator
import os
from flask.ext.cors import CORS, cross_origin

app = Flask(__name__)
app.config.from_object('config')
cors = CORS(app)

@app.route('/generatestl',methods=['POST'])
@cross_origin()
def index():
	r = Generator(xyzkey=str(request.form['xyz_key']), aws_key=app.config['AWS_ACCESS_KEY'],aws_secret=app.config['AWS_SECRET_KEY'],aws_bucket=app.config['S3_BUCKET'])
	response = r.create_STL_file()
	return response


@app.route('/test')
@cross_origin()
def index2():
    return str(1+1)

if __name__ == '__main__':
    app.run(host='0.0.0.0',debug=False)