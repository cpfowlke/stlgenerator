'''
Created on May 10, 2015

@author: cpfowlke
'''
from boto.s3.connection import S3Connection
from boto.s3.key import Key
import uuid
import os
import base64

class S3operations():
    '''
    This class allows the user to add/retrieve/delete a generated STL file to Amazon S3 services.
    
    '''
    def __init__(self,aws_key,aws_secret,aws_bucket):
        self.AWS_KEY = aws_key        
        self.AWS_SECRET = aws_secret
        self.OCC_BUCKET = aws_bucket
        self.dirpath = os.path.dirname(os.path.realpath(__file__))
        
    def S3filesave(self,stl_file_path):
        conn = S3Connection(self.AWS_KEY, self.AWS_SECRET)
        b = conn.get_bucket(self.OCC_BUCKET)
        k = Key(b)
        k.key = str(uuid.uuid1().int)+'.stl'
        k.set_contents_from_filename(stl_file_path, replace=False)
        url = k.generate_url(86400, 'GET')
        results={'code':'success',
                 'stlkey':str(k.key),
                 'url':str(url)}
        conn.close()
        
        return results

    def S3fileRetrieve(self,keyid):
        conn = S3Connection(self.AWS_KEY, self.AWS_SECRET)
        try:
            key = conn.get_bucket(self.OCC_BUCKET).get_key(keyid)
            tempfilepath=os.path.join(os.getcwd(),'temp.xyz')
            f = open(tempfilepath,'r')
            #message = key.get_file(temp)
            key.get_contents_to_filename('temp.xyz')
            message = base64.b64encode(f.read())
            f.close()
            #message = key.get_contents_as_file()
            response = {'code':'success',
                        'message':message}
            conn.close()
            return response
        except ValueError:
            response = {'code':'failure',
                          'message':'could not retieve file with key value '+keyid}
            conn.close()
            return response
    
    def S3fileDelete(self,keyid):
        conn = S3Connection(self.AWS_KEY,self.AWS_SECRET)
        b = conn.get_bucket(self.OCC_BUCKET)
        k = Key(b)
        k.key = keyid
        b.delete_key(k)
        conn.close()
    
    def listS3BucketContents(self):
        conn = S3Connection(self.AWS_KEY,self.AWS_SECRET)
        b = conn.get_bucket(self.OCC_BUCKET)
        for key in b.list():
            print "{name}\t{size}\t{modified}".format(
                    name = key.name,
                    size = key.size,
                    modified = key.last_modified,
                    )
    
