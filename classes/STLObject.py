
import time
import os
import sys
import numpy as np
import math as m
import base64
#from OCC.DataExchange.STL import STLExporter
#from OCC import *
from STL import STLExporter
from OCC.StlAPI import StlAPI_Writer
from OCC.BRepPrimAPI import *
from OCC.BRepAlgoAPI import *
from OCC.BRepBuilderAPI import *
from OCC.gp import *
from utils.filemanagement import S3operations as S3
class Generator():
    '''
    classdocs
    '''
    def __init__(self, xyzkey, aws_key,aws_secret,aws_bucket,scalefactor=1):
        '''
        Initialization of paths for file reading and molecule sizing
        '''
        ###Set the main paths 
        self.AWS_KEY = aws_key        
        self.AWS_SECRET = aws_secret
        self.OCC_BUCKET = aws_bucket
        self.running_dir = os.path.dirname(os.path.realpath(__file__))
        self.head_dir = os.path.abspath(os.pardir)
        self.xyz_key = xyzkey
        self.stl_key = ''
        #print(os.pardir)
        #print(os.path.join(os.getcwd(),'static'))
        self.static_path = os.path.join(os.getcwd(),'static')
        #self.static_path = os.path.join(os.pardir,'static')

        self.cov_radius = ''
        self.fudge = scalefactor
    
    def cov_radius_assignment(self,atype,radii,atom):
        for i in range(0,len(atype)):
            if atom==atype[i,0]:
                cov_radius=radii[i,0]
                break
            elif i==len(atype):
                print('Unknown element:',atom)
        return(cov_radius) 

    def database_read(self):
        #file_covradii=open(databases+'\\covalent_radii.txt','r')
        #print(self.static_path)
        #print(os.path.join(self.static_path,'covalent_radii.txt'))
        file_covradii = open(os.path.join(self.static_path,'covalent_radii.txt'),'r')
        lines=file_covradii.readlines()
        file_covradii.close()
        
        atype=[]
        radii=[]
        for line in lines:
            elements=line.split()
            atype.append(elements[0])
            radii.append(elements[1])
            
        atype=np.array(atype,dtype='str')
        radii=np.array(radii,dtype='float')
        atype.shape=(len(atype),1)
        radii.shape=(len(radii),1)
        return(atype,radii)

    def xyz_read(self):
        data = S3(self.AWS_KEY,self.AWS_SECRET,self.OCC_BUCKET).S3fileRetrieve(self.xyz_key)
        if data['code'] == 'success':
            #file_xyz=open(self.xyz_key,'r')
            lines = base64.b64decode(data['message']).split('\n')
    #Store all the atom types and the xyz coordinates
        counter=0
        atom_type=[]
        xposi=[]
        yposi=[]
        zposi=[]
        for line in lines:
            counter=counter+1
            if counter > (2):
                column=line.split()
                atom_type.append(column[0])
                xposi.append(column[1])
                yposi.append(column[2])
                zposi.append(column[3])
                
        return(atom_type,xposi,yposi,zposi)

    def transformation_matrix(self,xpos,ypos,zpos,alpha,beta,gamma):
        '''Calcuating the transformation matrix 
        Fractional coordinates (a-axis is collinear with the x-axis)'''

        alpha_rad=alpha*(np.pi/180)
        beta_rad=beta*(np.pi/180)
        gamma_rad=gamma*(np.pi/180)
        xpos_f=np.array(xpos,dtype='f')
        ypos_f=np.array(ypos,dtype='f')
        zpos_f=np.array(zpos,dtype='f')
        a=np.max(np.abs(xpos_f))
        b=np.max(np.abs(ypos_f))
        c=np.max(np.abs(zpos_f))
        all_coords=np.array([a,b,c])
        a=np.max(all_coords)
        b=np.max(all_coords)
        c=np.max(all_coords)
    
        c1 = c*m.cos(beta_rad)
        c2 = c*(m.cos(alpha_rad) - m.cos(gamma_rad) * m.cos(beta_rad))/m.sin(gamma_rad)
        c3 = (c**2 - c1**2 - c2**2)**0.5
        #C = [ a b*cos(gamma) c1 ; 0 b*sin(gamma) c2; 0 0 c3];
        C_mat=np.zeros([3,3])
        C_mat[0][0] = a
        C_mat[0][1] = b*m.cos(gamma_rad)
        C_mat[0][2] = c1
        C_mat[1][1] = b*m.sin(gamma_rad)
        C_mat[1][2] = c2
        C_mat[2][2] = c3
        C_mat_inv=np.linalg.inv(C_mat)
        #print C_mat
        #print C_mat_inv
        return(C_mat,C_mat_inv)

    def connectivity_mapping(self,natoms,radii,atom_type,atype,coord_frac,C_mat):
        #print(coord_frac[0][0])
        rvec=np.zeros([3,1])
        rx=np.zeros([natoms,natoms])
        ry=np.zeros([natoms,natoms])
        rz=np.zeros([natoms,natoms])
        dummy=np.zeros([3,1])
        conn_matrix=np.zeros([natoms,13])
        conn_matrix_char=np.zeros([natoms,6],dtype=('a3'))
        conn_matrix_nums=np.zeros([natoms,6],dtype=('a5'))
        expand = 9999999
        bond_fix = 0.4
        for i in range(natoms):
            index1=1
            index2=0
            atom1=atom_type[i]
            #This is extremely slow.... rad1=cov_radius_assignment(atom1)
            #c1=np.where(unique_list_sorted==atom1)
            #rad1=unique_rad_array[c1][0]
            rad1=self.cov_radius_assignment(atype,radii,atom1)
            conn_matrix[i][0]=i+1
            #print i
            #print i+1,rad1
            for j in range(natoms):
                atom2=atom_type[j]
                #This is extremely slow.... rad2=cov_radius_assignment(atom2)
                #c2=np.where(unique_list_sorted==atom2)
                #rad2=unique_rad_array[c2][0]
                rad2=self.cov_radius_assignment(atype,radii,atom2)
                #print j+1,rad2
               
                rx[i][j] = coord_frac[i,0] - coord_frac[j,0]
                ry[i][j] = coord_frac[i,1] - coord_frac[j,1]
                rz[i][j] = coord_frac[i,2] - coord_frac[j,2]
        
                #This includes PBCS...
                rx[i][j] = rx[i,j]- expand*round(rx[i,j]/expand)
                ry[i][j] = ry[i,j] - expand*round(ry[i,j]/expand)
                rz[i][j] = rz[i,j] - expand*round(rz[i,j]/expand)
        
                rvec[0][0] = rx[i,j]
                rvec[1][0] = ry[i,j]
                rvec[2][0] = rz[i,j]
                #print rvec
                rdummy = np.dot(C_mat,rvec) #Converting back to real coordinates
                #print rdummy
                r = (rdummy[0,0]**2 +  rdummy[1,0]**2 +  rdummy[2,0]**2)**0.5
                #print r
                #print r 
                if i != j:
                    #print r,(rad1+rad2)
                    if r < 10e-8:
                        print('WARNING THERE ARE DUPLICATE ATOMS. REVISE XYZ FILE ACCORDINGLY.')
                        #print j+1
                        #print r
                    if r <= (rad1+rad2)+bond_fix: #This means atom(i) is bonded to atom(j)
                        #print j+1
                        #print atom1,rad1,atom2,rad2,r[i,j] 
                        conn_matrix[i][index1]=j+1
                        index1=index1+1
                        conn_matrix[i][index1]=r
                        index1=index1+1
                        conn_matrix_char[i][index2]=atom_type[j]
                        conn_matrix_nums[i][index2]=str(j+1)
                        #print(j+1)
                        #conn_matrix_convert[i][index2]=str(j+1)
                        index2=index2+1
                        #stag_array=np.array([coord_sorted[i][0],coord_sorted[i][1],coord_sorted[i][2],coord_sorted[j][0],coord_sorted[j][1],coord_sorted[j][2]])
                        #stag_array.shape=(1,6)
                        #bonds_4_c4d=np.concatenate((bonds_4_c4d,stag_array),axis=0)
    
        return(conn_matrix,conn_matrix_nums,conn_matrix_char)

    def bond_maker(self,natoms,conn_matrix_nums):
        #print('NATOMS:',natoms)
        #print(conn_matrix_nums)
        bond_array=np.empty([1,2],dtype='int')
    
        for i in range(natoms):
            for j in range(6):
                if i == 0 and j == 0 and conn_matrix_nums[i,j] != '': #start it out
                    bond_array[0,0]=int(i+1)
                    bond_array[0,1]=int(conn_matrix_nums[i,j])
                elif conn_matrix_nums[i,j] != '': #append the rest
                    bond_array=np.append(bond_array,[[int(i+1),int(conn_matrix_nums[i,j])]],axis=0)
    
        bond_array=np.sort(bond_array)
        bond_array=self.unique_rows(bond_array)
        return(bond_array)

    def unique_rows(self,a):
        a = np.ascontiguousarray(a)
        unique_a = np.unique(a.view([('', a.dtype)]*a.shape[1]))
        return unique_a.view(a.dtype).reshape((unique_a.shape[0], a.shape[1]))
    
    def create_STL_file(self,file_name="temp"):
        '''Chains together the member function calls necessary to generate an stl file'''
        #Read in the databases (color, VDW radii, molecular weight, periodic table location, etc.)
        atype,radii=self.database_read()
        ###Read xyz file
        atom_type,xpos,ypos,zpos=self.xyz_read() #do file format checking to catch errors
        xposnpc=np.array(xpos,dtype='f')
        yposnpc=np.array(ypos,dtype='f')
        zposnpc=np.array(zpos,dtype='f')
        #Shift the coordinates to fall in the positive x,y,z places
        minxpos=np.abs(np.min(xposnpc))
        minypos=np.abs(np.min(yposnpc))
        minzpos=np.abs(np.min(zposnpc))
        xposnp=xposnpc+minxpos
        yposnp=yposnpc+minypos
        zposnp=zposnpc+minzpos
        natoms=len(atom_type)
        
        #generate connectivity matrix and bonds
        C_mat,C_mat_inv=self.transformation_matrix(xpos,ypos,zpos,90,90,90)
        coord_reg=np.vstack([xposnp,yposnp,zposnp])
        coord_reg=coord_reg.T
    
        coord_frac=np.dot(C_mat_inv,coord_reg.T)
        coord_frac=np.array(coord_frac.T)
        
        #f = open(os.path.join(os.path.abspath(os.pardir),'temp','frac_test.xyz'),'w')
        #f.write(str(14)+'\n\n')
        #for x in range(10):
        #    f.write('C '+str(coord_frac[x,0]*10)+' '+str(coord_frac[x,1]*10)+' '+str(coord_frac[x,2]*10)+'\n')
        #f.close()

        conn_matrix,conn_matrix_nums,conn_matrix_char=self.connectivity_mapping(natoms,radii,atom_type,atype,coord_frac,C_mat)

        #Remove duplicate bonds (possibly make this happen inside the connectivity matrix generator)
        bond_array=self.bond_maker(natoms,conn_matrix_nums)
        nbonds=len(bond_array)
    
        '''IS THIS COMPUTATION NECESSARY?'''
        #Print the C4D file to read
        #file=open(src+'\\C4D_'+str(name)+'.data','w')
        #f=open(os.path.join(os.path.abspath(os.pardir),'temp','C4D_'+self.xyz_key+'.data'),'w')
        #f.write('Atoms\n\n')
        #for k in range(natoms):
        #    f.write(str(atom_type[k])+' '+str(xposnpc[k])+' '+str(yposnpc[k])+' '+str(zposnpc[k])+'\n')
        #f.write('\nBonds\n\n')
        #for k in range(nbonds):
        #    f.write(str(bond_array[k,0])+' '+str(bond_array[k,1])+'\n')
        #f.close()

        #Generate the 3D image and save as the required file type
        #Shapeways takes STL, OBJ, X3D, DAE, Collada, and WRL file types
        atom_shape_list=[]
        bond_shape_list=[]

        #FIND WHICH PART OF THIS IS SLOW.  if its writing the file, then I have no idea what to do, except by reducing polygon counts
        #Draws all the atoms as spheres (I need to find a way of changing the polygon counts...)
        
        hshortener=0.1 #this is apparently needed due to overlapping polygons which the STL exporter does not like
        for atom in range(natoms):
            cradii=self.cov_radius_assignment(atype,radii,atom_type[atom])
            cradii=cradii/1.3 #included a fudge factor to scale down the atomic radii (need to make room for the bonds)
            sphere = BRepPrimAPI_MakeSphere(gp_Pnt(float(xposnp[atom])*self.fudge,float(yposnp[atom])*self.fudge,float(zposnp[atom])*self.fudge),cradii).Shape()
            atom_shape_list.append(sphere)
        
        #Draws all the cylinder bonds
        #print(bond_array)
        #print(len(bond_array))
        #print(nbonds)
        for bond in range(nbonds):
            index1=bond_array[bond,0]-1
            index2=bond_array[bond,1]-1
            #print index1,index2
            atom1=[float(xposnp[index1])*self.fudge,float(yposnp[index1])*self.fudge,float(zposnp[index1])*self.fudge]
            atom2=[float(xposnp[index2])*self.fudge,float(yposnp[index2])*self.fudge,float(zposnp[index2])*self.fudge]
            vector=[atom1[0]-atom2[0],atom1[1]-atom2[1],atom1[2]-atom2[2]]
            #print(vector)
            height=np.sqrt((float(atom1[0])-float(atom2[0]))**2+(float(atom1[1])-float(atom2[1]))**2+(float(atom1[2])-float(atom2[2]))**2)
            #print(height)
            cylinder = BRepPrimAPI_MakeCylinder(gp_Ax2(gp_Pnt(atom2[0],atom2[1],atom2[2]),gp_Dir(vector[0],vector[1],vector[2])),.2,height-hshortener).Shape()
            bond_shape_list.append(cylinder)
            
        #Fuse the list of atoms and bonds into one shape
        print('FUSING SHIT')
        atom_fuse_result = atom_shape_list.pop() #init loop
        counter=0
        while len(atom_shape_list)>0:
            counter+=1
            #print(counter)
            shape_to_add = atom_shape_list.pop()
            atom_fuse_result = BRepAlgoAPI_Fuse(atom_fuse_result, shape_to_add).Shape()
    
        bond_fuse_result = bond_shape_list.pop() #init loop
        counter=0
        while len(bond_shape_list)>0:
            counter+=1
            #print(counter)
            shape_to_add = bond_shape_list.pop()
            bond_fuse_result = BRepAlgoAPI_Fuse(bond_fuse_result, shape_to_add).Shape()
    
        fuse_result = BRepAlgoAPI_Fuse(atom_fuse_result, bond_fuse_result).Shape() 

        print('DONE FUSING SHIT')  
        
        #Export to a .stl to be read by 3D slicer program
        SCALE=gp_Trsf()
        SCALE.SetScaleFactor(7)
        fuse_result=BRepBuilderAPI_Transform(fuse_result,SCALE).Shape()
        
        #os.chdir(output)
        #os.chdir(self.head_dir,'temp')
        #STL Exporter doesn't seem to work
        #STL=STLExporter(filename=str(name)+'.stl',ASCIIMode=False)
        #STL.set_shape(fuse_result)
        #STL.write_file() 
        # Export to STL
        #stl_writer = StlAPI_Writer()
        #stl_writer.SetASCIIMode(True)
        #stl_name_path = os.path.join(self.head_dir,'temp',self.xyz_key+'_STL.stl')
        stl_name_path=os.path.join(os.getcwd(),self.xyz_key+'_STL.stl')
        #stl_writer.Write(fuse_result,stl_name_path)
        STL=STLExporter(stl_name_path,ASCIIMode=False)
        print('Started STLExporter')
        STL.set_shape(fuse_result)
        print('Set shape')
        STL.write_file() 
        print('Wrote file')
        #os.chdir(self.running_dir)
        #os.chdir(src)
        
        #upload into Amazon S3
        print('savingfile')
        response = S3(self.AWS_KEY,self.AWS_SECRET,self.OCC_BUCKET).S3filesave(stl_name_path)
        self.stl_key = response['stlkey']
        #print(response['stlkey'])
        #print(response['url'])
        os.remove(stl_name_path)
        print('done')
        return response['url']
        #return response